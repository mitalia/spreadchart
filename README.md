Generate a chart of Bund/BTP spread, divided by government.

Run as `./spread_chart.py < data.json`; works with both Python 2 and 3, requires matplotlib

Data from https://www.money.it/+-Spread-Btp-Bund-+
