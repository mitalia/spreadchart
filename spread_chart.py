#!/usr/bin/env python3
import json
import sys
from datetime import datetime
gvmts_data = [
        ((1998,10,21), "D'Alema I"),
        ((1999,12,22), "D'Alema II"),
        ((2000, 4,26), "Amato II"),
        ((2001, 6,11), "Berlusconi II"),
        ((2005, 4,23), "Berlusconi III"),
        ((2006, 5,17), "Prodi II"),
        ((2008, 5, 8), "Berlusconi IV"),
        ((2011,11,16), "Monti"),
        ((2013, 4,28), "Letta"),
        ((2014, 2,22), "Renzi"),
        ((2016,12,12), "Gentiloni"),
        ((2018, 6, 1), "Conte I"),
        ((2019, 9, 5), "Conte II"),
        ((2100, 1, 1), "???"),
        ]
gvts = [(datetime(*s), n, [], []) for s,n in gvmts_data]
data = [(datetime.fromtimestamp(ts/1000.), v) for ts,v in json.load(sys.stdin)["data"]]
data.sort()
for t,value in data:
    i = 0
    while t > gvts[i][0]:
        i+=1
    i -= 1
    gvts[i][2].append(value)
    gvts[i][3].append(t)

from matplotlib import pyplot as plt
for s, n, ys, xs in gvts:
    if len(xs) == 0:
        continue
    plt.plot(xs, ys, label = n)
plt.legend()
plt.show()
